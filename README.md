# xxxd 0.1

[![made with Ruby](https://img.shields.io/badge/made%20with-Ruby-CC342D.svg?logo=ruby)](https://en.wikipedia.org/wiki/Ruby_(programming_language))
[![made with Python](https://img.shields.io/badge/made%20with-Python-F9DC3E.svg?logo=python&logoColor=white)](https://en.wikipedia.org/wiki/Python_(programming_language))
[![ade with Shell Script](https://img.shields.io/badge/made%20with-Shell%20Script-1F1A1C.svg?logo=gnubash&logoColor=white)](https://en.wikipedia.org/wiki/Shell_script)
[![licensed CC0 1.0](https://img.shields.io/badge/licensed-CC0%201.0-44B987.svg?logo=unlicense&logoColor=white)](https://creativecommons.org/publicdomain/zero/1.0/deed.en)

## What's this?

This is a project I prepared to try my hand at using git.

As of September 2023, I don't really understand git, GitHub or GitLab.

## What happens if it is executed?

If you run xxxd 0.1 without any options, it will display a list of information (size and SHA256 checksum value) for the files in the current directory.

The results are similar to the following hash_list.txt.  
https://gitlab.com/-/snippets/2416279

I will try to make the Ruby and Python editions output the same results as much as possible.

Shell script edition may not follow the functionalities of other editions.

## Future plans

Additional fetures such as fine-grained control by specifying options and processing data from STDIN are planned.

But since it is basically a tool for my own use, and since I have created a repository to try out git, there is a possibility that there will be frequent and sudden changes in specifications.

There is also a possibility of abrupt and inconsistent feature additions.

## Miscellaneous notes

### about the option "--out=btcdh"

"btcdh" means "bitcoin double hash".

For example, in legacy transactions, a style that existed before SegWit appeared in 2017, applying this "btcdh" process to the raw data of a transaction results in a transaction ID (txid).

Let's actually check this.

1. Create a new directory to store raw bitcoin transaction data.

```
$ mkdir txid_test
$ cd txid_test
$ wget -q https://gitlab.com/cleemy-desu-wayo/xxxd0.1/-/raw/4865500e9c522214df373d27c3f65a626d154a2b/xxxd0.1.rb
$ wget -q https://blockstream.info/api/tx/8507fb93e06572f278284028c23826407ca1dbeed13a1b7f514d2738e0978227/raw -O tx_8507fb93e06572f278284028c23826407ca1dbeed13a1b7f514d2738e0978227.dat
$ wc -c *
 299 tx_8507fb93e06572f278284028c23826407ca1dbeed13a1b7f514d2738e0978227.dat
1891 xxxd0.1.rb
2190 total
```

2. If you run xxxd with --out=btcdh when there are only two files in the current directory, xxxd.0.1.rb and raw transaction data, you should get something like the following.

```
$ chmod 705 xxxd0.1.rb
$ ./xxxd0.1.rb --out=btcdh
#####################################################################
# hash list
# 
# 2023-09-28 19:31:06 UTC
# 
# format:
#   <byte size>,<btcdh>
# 
#####################################################################
299,8507fb93e06572f278284028c23826407ca1dbeed13a1b7f514d2738e0978227
```
