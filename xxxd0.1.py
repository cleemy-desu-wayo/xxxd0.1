#!/usr/bin/env python3
#
# ==========================================================================
# xxxd0.1.py -- version 0.0.99.20231002
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/xxxd0.1
# ==========================================================================

import os
import sys
import datetime
import hashlib
import re

now_str = datetime.datetime.now(datetime.timezone.utc).strftime('%Y-%m-%d %H:%M:%S %Z')

argv = sys.argv

output_type = 'sha2-256'

if len(sys.argv) >= 2:
    if sys.argv[1] == '--out=sha2-256':
        output_type = 'sha2-256'
    elif sys.argv[1] == '--out=base64':
        import base64
        output_type = 'base64'
    elif sys.argv[1] == '--out=btcdh':
        output_type = 'btcdh'
    elif sys.argv[1] == '--out=exec':
        import base64
        import shlex   # do not rely completely on this
        output_type = 'exec'
    elif sys.argv[1] == '--out=hex':
        output_type = 'hex'
    elif re.match(r'.*s.*256', sys.argv[1]):
        print('ERROR: invalid option... did you mean sha2-256?', file=sys.stderr)
        sys.exit(1)
    else:
        print('ERROR: invalid option', file=sys.stderr)
        sys.exit(1)

header = f'''
#####################################################################
# hash list
# 
# {now_str}
# 
# format:
#   <byte size>,<{output_type}>
# 
#####################################################################
'''[1:-1]

if output_type == 'exec':
  print('#!/bin/sh')

print(header)

mode_exec_header = '''

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

base64_decode() {
  if type base64 > /dev/null 2>&1; then
    #echo 'mode: base64' 1>&2   # debug
    base64 -d
  elif type openssl > /dev/null 2>&1; then
    #echo 'mode: openssl' 1>&2   # debug
    openssl base64 -d
  else
    echo 'mode: error' 1>&2
    exit 1
  fi
}

'''[1:-1]

if output_type == 'exec':
  print(mode_exec_header)

def convert_data(data_source, file_name):
    if output_type == 'sha2-256':
        return hashlib.sha256(data_source).hexdigest()
    elif output_type == 'base64':
        return base64.b64encode(data_source).decode()
    elif output_type == 'btcdh':
        tmp = data_source
        tmp = hashlib.sha256(tmp).digest()
        tmp = hashlib.sha256(tmp).digest()
        tmp = tmp[::-1].hex()
        return tmp
    elif output_type == 'exec':
        b64_str = base64.b64encode(data_source).decode()
        b64_multi_lines = re.sub('(.{1,60})', '\\1\n', b64_str)
        tmp = file_name.replace('\n', r'\n')
        quoted_file_name = shlex.quote(tmp)
        if not re.match('\'.*\'$', quoted_file_name):
            quoted_file_name = "'" + quoted_file_name + "'"
        tmp = ''
        tmp += f'# generate {quoted_file_name}\n'
        tmp += 'cat << _EOS | base64_decode > ' + quoted_file_name + '\n'
        tmp += b64_multi_lines
        tmp += '_EOS' + '\n'
        return tmp
    elif output_type == 'hex':
        return data_source.hex()
    else:
        print('ERROR: unknown output type', file=sys.stderr)
        sys.exit(1)

def get_fileinfo_from_file(file_name):
    with open(file_name, mode='rb') as f:
        bin_data = f.read()
        return len(bin_data), convert_data(bin_data, file_name)

file_list = os.listdir('./')
file_list.sort()

for file_name in file_list:
    if not os.path.isfile(os.path.join('./', file_name)):       # ignore dir
        continue
    if not os.access(os.path.join('./', file_name), os.R_OK):   # ignore unreadable files
        continue
    if ('\n' in file_name):                                     # ignore files with filenames containing '\n'
        continue
    if not file_name.isascii():                                 # ignore files with invalid filenames
        continue
    if not re.match(r'[-=_%.0-9a-zA-Z]+$', file_name):          # ignore files with invalid filenames
        continue
    if file_name.startswith('.'):      # ignore dotfiles
        continue
    if file_name.startswith('xxxd'):   # ignore xxxd*
        continue

    file_info = get_fileinfo_from_file(file_name)
    if output_type == 'exec':
        print(file_info[1])
    else:
        print(str(file_info[0]) + ',' + file_info[1])
