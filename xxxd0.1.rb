#!/usr/bin/env ruby
#
# ==========================================================================
# xxxd0.1.rb -- version 0.0.99.20231002.1
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/xxxd0.1
# ==========================================================================

require 'digest'

now_str = Time.now.utc.strftime('%Y-%m-%d %H:%M:%S %Z')

output_type = 'sha2-256'

case ARGV[0]
when '--out=sha2-256'
  output_type = 'sha2-256'
when '--out=base64'
  require 'base64'
  output_type = 'base64'
when '--out=btcdh'
  output_type = 'btcdh'
when '--out=exec'
  require 'base64'
  require 'shellwords'   # do not rely completely on this
  output_type = 'exec'
when '--out=hex'
  output_type = 'hex'
when /s.*256/
  raise 'ERROR: invalid option... did you mean sha2-256?'
when nil
  nil
else
  raise 'ERROR: invalid option'
end

puts '#!/bin/sh' if output_type == 'exec'

header = "
#####################################################################
# hash list
# 
# #{now_str}
# 
# format:
#   <byte size>,<#{output_type}>
# 
#####################################################################
".strip

puts header

mode_exec_header = "
LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

base64_decode() {
  if type base64 > /dev/null 2>&1; then
    #echo 'mode: base64' 1>&2   # debug
    base64 -d
  elif type openssl > /dev/null 2>&1; then
    #echo 'mode: openssl' 1>&2   # debug
    openssl base64 -d
  else
    echo 'mode: error' 1>&2
    exit 1
  fi
}

"

puts mode_exec_header if output_type == 'exec'

def convert_data(bin_data, file_name, output_type)
  case output_type
  when 'sha2-256'
    Digest::SHA256.hexdigest(bin_data)
  when 'base64'
    Base64.strict_encode64(bin_data)
  when 'btcdh'
    tmp = bin_data
    tmp = Digest::SHA256.digest(tmp)
    tmp = Digest::SHA256.digest(tmp)
    tmp.reverse.unpack("H*")[0]
  when 'exec'
    b64_str_multiple_lines = Base64.encode64(bin_data)
    tmp = file_name.gsub("\n", '\n')
    escaped_file_name = Shellwords.escape(tmp)
    tmp = ''
    tmp = tmp + "# generate #{escaped_file_name}\n"
    tmp = tmp + 'cat << _EOS | base64_decode > ' + escaped_file_name + "\n"
    tmp = tmp + b64_str_multiple_lines
    tmp = tmp + "_EOS\n\n"
    tmp
  when 'hex'
    bin_data.unpack("H*")[0]
  end
end

def get_fileinfo_from_file(file_name, output_type)
  bin_data = File.binread(file_name)
  [bin_data.bytesize, convert_data(bin_data, file_name, output_type)]
end

Dir.glob('*') do |file_name|
  next unless File.file?(file_name)                       # ignore dir
  next unless File.readable?(file_name)                   # ignore unreadable files
  next if     file_name.include?("\n")                    # ignore files with filenames containing "\n"
  next unless file_name.ascii_only?                       # ignore files with invalid filenames
  next unless file_name.match?(/\A[-=_%.0-9a-zA-Z]+\z/)   # ignore files with invalid filenames
  next if     file_name.start_with?('.')                  # ignore dotfiles
  next if     file_name.start_with?('xxxd')               # ignore xxxd*

  file_info = get_fileinfo_from_file(file_name, output_type)
  case output_type
  when 'exec'
    puts "#{file_info[1]}"
  else
    puts "#{file_info[0]},#{file_info[1]}"
  end
end
