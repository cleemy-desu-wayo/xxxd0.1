#!/bin/sh
#
# ==========================================================================
# xxxd0.1.sh -- version 0.0.99.20230924
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/xxxd0.1
# ==========================================================================
#
# * requirements:
#     * sha256sum (GNU coreutils) or openssl command (LibreSSL is also possible)
#     * built-in type command (maybe this script does not work with posh)
#

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

get_hash() {
  if type sha256sum > /dev/null 2>&1; then
    sha256sum | awk '{print $1}'
  elif type openssl > /dev/null 2>&1; then
    openssl sha256 | awk '{print $NF}'
  else
    echo 'ERROR' 1>&2
    exit 1
  fi
}

now_str=$(date "+%Y-%m-%d %H:%M:%S %Z" 2> /dev/null)
if [ -z "${now_str}" ]; then
  now_str=$(date)
fi

echo '#####################################################################'
echo '# hash list'
echo '# '
printf '# %s\n' "${now_str}" | head -1
echo '# '
echo '# format:'
echo '#   <byte size>,<sha256sum>'
echo '# '
echo '#####################################################################'

ls -a | sort | while read file_name ; do
  is_valid_file=0

  # accept a file that is a regular file and is readable
  if [ -f "${file_name}" ] && [ -r "${file_name}" ]; then
    is_valid_file=1
  fi

  # ignore dotfiles
  if printf '%s\n' "${file_name}" | grep -q '^\.' ; then
    is_valid_file=0
  fi
  # ignore xxxd*
  if printf '%s\n' "${file_name}" | grep -q '^xxxd' ; then
    is_valid_file=0
  fi

  if [ "${is_valid_file}" = '1' ]; then
    cat "${file_name}" | wc -c | sed 's/[^0-9]//g' | tr -d '\012'
    printf '%s' ','
    cat "${file_name}" | get_hash | sed 's/[^0-9a-f]//g' | tr -d '\012'
    echo
  fi
done

exit 0
